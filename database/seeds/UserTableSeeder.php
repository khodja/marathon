<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('type', 'admin')->first();

        $admin = new User;
        $admin->name = 'admin';
        $admin->last_name = 'admin';
        $admin->phone = 990000000;
        $admin->extra_phone = 990000000;
        $admin->gender = 0;
        $admin->country = 'admin';
        $admin->city = 'admin';
        $admin->email = 'admin@gmail.com';
        $admin->size = 'XXL';
        $admin->level = 'A';
        $admin->distance_id = 1;
        $admin->is_paid = 1;
        $admin->invalid = 0;
        $admin->birthday = \Carbon\Carbon::today();
        $admin->password = bcrypt('secret');
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
