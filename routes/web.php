<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
    function() {
    Auth::routes(['register' => false]);
    Route::get('/', 'PageController@index');
    Route::get('/runners', 'PageController@runners');
    Route::get('/test', 'PageController@test');
    Route::get('/auth', 'PageController@login');
    Route::get('/information', 'PageController@information');
    Route::get('/contacts', 'PageController@contacts');
    Route::get('/about', 'PageController@about');
    Route::get('/summary', 'PageController@summary');
    Route::get('/program', 'PageController@program_first');
    Route::get('/program/second', 'PageController@program_second');
    Route::get('/program/third', 'PageController@program_third');
    Route::get('/program/fourth', 'PageController@program_fourth');
    Route::get('/interesting', 'PageController@interesting');
    Route::get('/register', 'PageController@register');
    Route::get('/register/payment', 'PageController@payment');
    Route::get('/register/success', 'PageController@success');
    Route::get('/profile', 'PageController@profile');
    Route::get('/publication', 'PageController@publication');
    Route::get('/payment/octo', 'PaymentController@octo');
    Route::get('/payment/uzcard', 'PaymentController@uzcard');

    Route::post('/auth', 'AuthController@register');
    Route::post('/user/login', 'AuthController@login')->name('frontier');
    Route::get('/logout', 'AuthController@logout');
    Route::post('update', 'AuthController@update');
});

Route::group(['middleware' => ['role:admin'] ], function () {
    Route::get('/dashboard', 'PanelController@index');
    Route::get('/pages', 'PanelController@pages');
    Route::get('/pages/{id}', 'PanelController@editPage');
    Route::put('/pages/{id}', 'PanelController@updatePage');

    Route::get('/resources', 'PanelController@resources');
    Route::get('/promo', 'PromoController@index');
    Route::get('/import', 'PanelController@import');
    Route::post('/custom', 'PanelController@custom');
    Route::get('/create/promo', 'PromoController@create');
    Route::post('/store/promo', 'PromoController@store');

    Route::get('/distance', 'DistanceController@index');
    Route::get('/create/distance', 'DistanceController@create');
    Route::get('/edit/distance/{id}', 'DistanceController@edit');
    Route::post('/store/distance', 'DistanceController@store');
    Route::put('/update/distance/{id}', 'DistanceController@update');

    Route::get('/create/resource', 'PanelController@create');
    Route::post('/store/resource', 'PanelController@store');
    Route::delete('/delete/resource/{id}', 'PanelController@destroy');
});
