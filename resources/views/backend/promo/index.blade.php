@extends('layouts.backend')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Промокоды</h3>
        <a class="btn btn-outline-success" href="{{ action('PromoController@create') }}">Добавить</a>
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>No</th>
            <th>Промокод</th>
            <th>Пользователь</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $key => $datas)
        <tr>
            <td>{{ $key }}</td>
            <td>{{$datas->name }}</td>
            <td>{{$datas->user ? $datas->user->name : null }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
</div>
@endsection
