@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Дистанцию</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('DistanceController@store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="text">Дистанция (км)</label>
                            <input type="text" id="text" class="form-control" checked="checked" name="long">
                        </div>
                        <div class="form-group">
                            <label for="text1">Место проведения (Адресс)</label>
                            <input type="text" id="text1" class="form-control" checked="checked" name="place">
                        </div>
                        <div class="form-group">
                            <label for="date">Дата Старта</label>
                            <input type="date" id="date" class="form-control" checked="checked" name="date">
                        </div>
                        <div class="form-group">
                            <label for="text2">Время</label>
                            <input type="text" id="text2" class="form-control" checked="checked" name="time">
                        </div>
                        <div class="form-group">
                            <label for="text3">Цена(UZS)</label>
                            <input type="text" id="text3" class="form-control" checked="checked" name="price">
                        </div>
                        <div class="form-group">
                            <label for="text4">Цена (USD)</label>
                            <input type="text" id="text4" class="form-control" checked="checked" name="price_usd">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
});
</script>
@endsection
