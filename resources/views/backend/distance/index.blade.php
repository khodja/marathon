@extends('layouts.backend')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Дистанции</h3>
{{--        <a class="btn btn-outline-success" href="{{ action('DistanceController@create') }}">Добавить</a>--}}
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Дата старта</th>
            <th>Время</th>
            <th>Дистанция</th>
            <th>Место</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td>{{ \Carbon\Carbon::parse($datas->date)->locale('ru')->isoFormat('D MMMM') }}</td>
            <td>{{$datas->time }}</td>
            <td>{{$datas->long }}</td>
            <td>{{$datas->place }}</td>
            <td>
                <a class="btn btn-outline-primary" href="{{action('DistanceController@edit',$datas->id)}}">Изменить</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
</div>
@endsection
