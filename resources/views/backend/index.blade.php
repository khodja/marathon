@extends('layouts.backend')

@section('content')
<div class="my-3 my-md-5">
    <div class="container-fluid">
    <div class="page-header justify-content-between">
        <h1 class="page-title">
        Марафон
        </h1>
        <a href="{{action('PanelController@create')}}" class="btn btn-success">Добавить ресурс</a>
    </div>
    <div class="row row-cards">
        <div class="col-md-6 col-lg-12">
        <div class="card">
            <div class="card-header justify-content-between">
            <h3 class="card-title">
                Пользователи
                <a href="{{ action('PanelController@import') }}" class="btn btn-info">Все Excel</a>
            </h3>
                <form class="text-center" action="{{ action('PanelController@custom') }}" method="POST">
                    @csrf
{{--                    <select name="size" id="size">--}}
{{--                        <option value="XXL">XXL</option>--}}
{{--                        <option value="XL">XL</option>--}}
{{--                        <option value="L">L</option>--}}
{{--                        <option value="M">M</option>--}}
{{--                        <option value="S">S</option>--}}
{{--                        <option value="XS">XS</option>--}}
{{--                    </select>--}}
                    <select name="is_paid" id="is_paid">
                        <option value="1">Оплатившие</option>
                        <option value="0">Неоплатившие</option>
                    </select>
                    <select name="distance" id="distance">
                        @foreach($distance as $item)
                        <option value="{{$item->id}}">{{$item->long}}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-info">
                        Excel
                    </button>
                </form>
            </div>
            <div class="table-responsive">
<table class="table card-table table-vcenter text-nowrap">
        <thead>
                <tr>
                    <th class="w-1">№</th>
                    <th>Email</th>
                    <th>Номер телефона</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Дата рождения</th>
                    <th>Пол</th>
                    <th>Страна</th>
                    <th>Город</th>
                    <th>Экстренный контакт</th>
                    <th>Размер футболки</th>
                    <th>Промокод</th>
                    <th>Дистанция</th>
                    <th>Планируемое время</th>
                    <th>Инвалидность</th>
                    <th>Оплата</th>
                    <th>Цена</th>
                    <th>Дата регистрации</th>
                </tr>
        </thead>
        <tbody>
        @foreach($users as $key => $datas)
        @if($datas->id != 1)
        <tr>
        <td>
            {{$key}}
        </td>
        <td>
            {{$datas->email}}
        </td>
        <td>
            {{$datas->phone}}
        </td>
        <td>
            {{$datas->name}}
        </td>
        <td>
            {{$datas->last_name}}
        </td>
        <td>
            {{$datas->birthday}}
        </td>
        <td>
            {{$datas->gender == 0 ? 'Муж': 'Жен'}}
        </td>
        <td>
            {{$datas->country}}
        </td>
        <td>
            {{$datas->city}}
        </td>
        <td>
            {{$datas->extra_phone}}
        </td>
        <td>
            {{$datas->size}}
        </td>
        <td>
            {{$datas->promo}}
        </td>
        <td>
            {{$datas->distance->long}}
        </td>
        <td>
            {{$datas->level}}
        </td>
        <td>
            {{$datas->invalid ? 'Есть' : ' '}}
        </td>
        <td>
            {{$datas->is_paid ? 'Оплатил' : 'Не олатил'}}
        </td>
        <td>
            {{$datas->distance->price.' сум'}}
        </td>
        <td>
            {{$datas->created_at->isoFormat('D MMMM HH:mm')}}
        </td>
        </tr>
        @endif
        @endforeach
        </tbody>
    </table>

            </div>
        </div>
        </div>
    </div>
</div>
</div>
@endsection
