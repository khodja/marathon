@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Ресурс</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('PanelController@store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <ul class="nav nav-tabs" style="text-transform: uppercase; padding-left: 20px;">
                          @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $key)
                            <li class="nav-item"><a href="#{{ $localeCode }}" style="padding: 0 0 10px 6px;" class="nav-link  text-center @if($localeCode =='ru') active @endif">{{ $localeCode }}</a></li>
                          @endforeach
                        </ul>
                        <div class="tab-content">
                          @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $key)
                            <div id="{{ $localeCode }}" class="tab-pane fade @if($localeCode =='ru') in active show @endif" style="padding-top: 20px;">
                                <div class="form-group">
                                    <label for="title_{{ $localeCode  }}">Название <span style="text-transform: uppercase;">{{ $localeCode  }}</span></label>
                                    <input name="title_{{ $localeCode  }}" @if($localeCode == 'ru') required  @endif type="text" class="form-control" id="title_{{ $localeCode  }}" placeholder="Введите Название Uz">
                                </div>
                                <div class="form-group">
                                    <label for="desc_{{ $localeCode  }}">Описание <span style="text-transform: uppercase;">{{ $localeCode  }}</span></label>
                                    <textarea name="desc_{{ $localeCode  }}" class="form-control" id="desc_{{ $localeCode  }}" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                          @endforeach
                        </div>
                        <div class="form-group">
                            Выберите категорию
                            <select name="category_id" class="form-control">
                                @foreach( $data as $datas )
                                    <option value="{{ $datas->id }}">{{ $datas->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="image">Фотография (400x600)</label>
                            <input type="file" id="image" class="form-control" name="image" required>
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
});
</script>
@endsection
