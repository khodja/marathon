@extends('layouts.frontend')
@section('content')
<div class="text-center">
        <div class="register__title">@lang('main.register')</div>
        <div class="register">
            <div class="container">
                <div class="row">
                <div class="col-md-9">
                    <div class="whitespace">
                        <div class="register__status">
                            <div class="register__status--wrapper text-left">
                                1. @lang('main.registration.1')
                            </div>
                            <div class="register__status--wrapper active">
                                2. @lang('main.registration.2')
                            </div>
                            <div class="register__status--wrapper text-right">
                                3. @lang('main.registration.3')
                            </div>
                        </div>

                        <div class="register__form">
                            <div class="flexy">
                                <a href="{{action('PaymentController@uzcard')}}">
                                    <img src="{{ asset('/img/octo.png') }}" />
                                    <span>UZCARD</span>
                                </a>
                                <a href="{{action('PaymentController@octo')}}">
                                    <img src="{{ asset('/img/octo.png') }}" />
                                    <span>Visa/Mastercard</span>
                                </a>
                            </div>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 hidden-sm">
                    <div class="whitespace-right">
                        <div class="register__right pl-30">
                            @lang('main.registration.pay')
                        </div>
                        <div class="register__price pl-30" id="main_price">
                            {{$data->price}} @lang('main.registration.sum')
                        </div>
                        <div class="register__payment">
                            <div class="register__payment--text pl-30">
                                @lang('main.registration.method'):
                            </div>
                            <div class="flexer">
                                <img src="{{ asset('/img/visa.svg')  }}" />
                                <img src="{{ asset('/img/pay1.svg')  }}" />
                                <img src="{{ asset('/img/pay2.svg')  }}" />
                                <img src="{{ asset('/img/pay3.svg')  }}" />
                                <img src="{{ asset('/img/pay4.svg')  }}" />
                            </div>
                        </div>
                        <div class="register__right pl-30">
                            @lang('main.registration.about')
                        </div>
                        <div class="register__info pl-30">
                            <div class="left">@lang('main.registration.distances'):</div>
                            <div class="right" id="main_long">{{$data->long}}</div>
                        </div>
                        <div class="register__info pl-30">
                            <div class="left">@lang('main.registration.start'):</div>
                            <div class="right" id="main_date">{{\Carbon\Carbon::parse($data->date)->locale(app()->getLocale())->isoFormat('D MMMM')}}</div>
                        </div>
                        <div class="register__info pl-30">
                            <div class="left">@lang('main.registration.place'):</div>
                            <div class="right" id="main_place">{{ app()->getLocale() == 'ru' ? $data->place : (app()->getLocale() == 'en' ? $data->place_en : $data->place_uz)  }}</div>
                        </div>
                        <div class="register__info pl-30">
                            <div class="left">@lang('main.registration.times'):</div>
                            <div class="right" id="main_time">{{$data->time}}</div>
                        </div>
                </div>
                </div>
                </div>
            </div>
        </div>
</div>
@endsection
