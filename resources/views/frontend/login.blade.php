@extends('layouts.app')

@section('content')
<div class="register mt-50 mb-60">
    <div class="container">
        <div class="col-md-6 col-md-push-3 py-layout pb-10 pt-10" style="box-shadow: 0px 4px 50px rgba(0, 0, 0, 0.1);">
            <div class="authorize">
                <div class="col-md-6">
                    <div class="auth__title">
                        @lang('main.profile.cabinet')
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <img src="{{ asset('img/profile-green.svg') }}" alt="">
                </div>
                <div class="col-md-12">
                    <form action="{{ route('frontier') }}" method="POST">
                        @csrf
                        <label for="email">E-mail</label>
                        <input type="email" id="email" name="email">
                        <label for="password">@lang('main.registration.password')</label>
                        <input type="password" id="password" name="password">
                        <a href="{{ route('password.request') }}">@lang('main.forget')</a>
                        <br>
                        <button class="content__btn">@lang('main.login')</button>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection
