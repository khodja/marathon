@extends('layouts.frontend')
@section('content')
<div class="contacts__title">
        @lang('main.nav.3')
</div>
<div class="about--info">
    <div class="container">
        <div class="about__summary">
            <div class="about__program" style="display: flex;flex-wrap: wrap;justify-content: space-around;">
                <a href="{{action('PageController@program_first')}}" @if(request()->is('*program')) class="active" @endif >30 октября</a>
                <a href="{{action('PageController@program_second')}}" @if(request()->is('*program/second')) class="active" @endif>1 ноября</a>
                <a href="{{action('PageController@program_third')}}" @if(request()->is('*program/third')) class="active" @endif>2 ноября</a>
                <a href="{{action('PageController@program_fourth')}}" @if(request()->is('*program/fourth')) class="active" @endif>3 ноября</a>
            </div>
        </div>
    {!! $data->text !!}
    </div>
</div>
@endsection
