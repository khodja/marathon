<section class="popular desktop">
        <div class="container cont-fix">
            <h3 class="popular__h3 text-center">Популярные рестораны</h3>        
            <div class="flex">
                <div class="popular__wrapper">
                    <div class="popular__wrapper-1">
                        <img src="{{ asset('/img/brand.png') }}" class="popular__wrapper-img">
                        <div class="popular__wrapper-text">
                            <h2 class="popular__title">Bistro24</h2> 
                            <p class="popular__p">Европейская</p>               
                        </div>
                        <div class="popular__wrapper-price">
                            <img src="{{ asset('/img/popular.svg') }}"><h2 class="popular__price">12 000 сум</h2> 
                            <p class="popular__long pt-fix">4.2 км</p>               
                        </div>                
                    </div>
                </div>
                <div class="popular__wrapper">
                    <div class="popular__wrapper-1">
                        <img src="{{ asset('/img/brand.png') }}" class="popular__wrapper-img">
                        <div class="popular__wrapper-text">
                            <h2 class="popular__title">Bistro24</h2> 
                            <p class="popular__p">Европейская</p>               
                        </div>
                        <div class="popular__wrapper-price">
                            <img src="{{ asset('/img/popular.svg') }}"><h2 class="popular__price">12 000 сум</h2> 
                            <p class="popular__long pt-fix">4.2 км</p>               
                        </div>                
                    </div>
                </div>
                <div class="popular__wrapper">
                    <div class="popular__wrapper-1">
                        <img src="{{ asset('/img/brand.png') }}" class="popular__wrapper-img">
                        <div class="popular__wrapper-text">
                            <h2 class="popular__title">Bistro24</h2> 
                            <p class="popular__p">Европейская</p>               
                        </div>
                        <div class="popular__wrapper-price">
                            <img src="{{ asset('/img/popular.svg') }}"><h2 class="popular__price">12 000 сум</h2> 
                            <p class="popular__long pt-fix">4.2 км</p>               
                        </div>                
                    </div>
                </div>            
                <div class="popular__wrapper">
                    <div class="popular__wrapper-1">
                        <img src="{{ asset('/img/brand.png') }}" class="popular__wrapper-img">
                        <div class="popular__wrapper-text">
                            <h2 class="popular__title">Bistro24</h2> 
                            <p class="popular__p">Европейская</p>               
                        </div>
                        <div class="popular__wrapper-price">
                            <img src="{{ asset('/img/popular.svg') }}"><h2 class="popular__price">12 000 сум</h2> 
                            <p class="popular__long pt-fix">4.2 км</p>               
                        </div>                
                    </div>
                </div> 
                <div class="popular__wrapper">
                    <div class="popular__wrapper-1">
                        <img src="{{ asset('/img/brand.png') }}" class="popular__wrapper-img">
                        <div class="popular__wrapper-text">
                            <h2 class="popular__title">Bistro24</h2> 
                            <p class="popular__p">Европейская</p>               
                        </div>
                        <div class="popular__wrapper-price">
                            <img src="{{ asset('/img/popular.svg') }}"><h2 class="popular__price">12 000 сум</h2> 
                            <p class="popular__long pt-fix">4.2 км</p>               
                        </div>                
                    </div>
                </div>                                     
                <div class="popular__wrapper">
                    <div class="popular__wrapper-1">
                        <img src="{{ asset('/img/brand.png') }}" class="popular__wrapper-img">
                        <div class="popular__wrapper-text">
                            <h2 class="popular__title">Bistro24</h2> 
                            <p class="popular__p">Европейская</p>               
                        </div>
                        <div class="popular__wrapper-price">
                            <img src="{{ asset('/img/popular.svg') }}"><h2 class="popular__price">12 000 сум</h2> 
                            <p class="popular__long pt-fix">4.2 км</p>               
                        </div>                
                    </div>
                </div>            
            </div>
            </div>
            <button class="popular__btn">Показать еще</button>
    </section>