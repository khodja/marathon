<footer class="footer">
    <div class="container">
        <div class="col-md-4">
            @lang('main.footer.fond')
                <br><br>
                @lang('main.footer.mail') <br>
                <a href="mailto:halfmarathon@acdf.uz" class="mailed" target="_blank">halfmarathon@acdf.uz.</a><br><br>
                @lang('main.footer.phone')
                +99871 207 40 50
        </div>
        <div class="col-md-4 text-right">
            <p class="text-left ib">
                @lang('main.footer.social')
            </p>
        </div>
        <div class="col-md-4">
            <div class="footer__socials text-center">
                <a href="https://www.facebook.com/samarkandhalfmarathon/" target="_blank"><img src="{{ asset('img/face.svg')  }}" /></a>
                <a href="https://www.instagram.com/samarkandhalfmarathon/" target="_blank"><img src="{{ asset('img/insta.svg')  }}" /></a>
                <a href="https://t.me/joinchat/AAAAAFiSBZ0S2cRwdFnTQw" target="_blank"><img src="{{ asset('img/tele.svg')  }}" /></a>
            </div>
        </div>
    </div>
</footer>
