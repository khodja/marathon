<section class="categories-info">
    <div class="container cont-fix">
        <div class="col-md-12 index">
            <h2 class="categories-info__h2">А в приложении все еще удобнее</h2>
        </div>
         <div class="col-md-3 lg-hidden index text-center">
            <button class="useful__btn"><img src="{{ asset('/img/playmarket.svg') }}">Загрузить для Android</button>
         </div>
         <div class="col-lg-6 col-md-12 index text-center">
            <div class="categories-info__phone">
               <img src="{{ asset('/img/phone1.png') }}">
            </div>
            <div class="categories-info__phone">
               <img src="{{ asset('/img/phone2.png') }}">
            </div>
            <button class="useful__btn dispnone"><img src="{{ asset('/img/playmarket.svg') }}">Загрузить для Android</button>            
            <button class="useful__btn mb-20 dispnone"><img src="{{ asset('/img/appstore.svg') }}">Загрузить для ApPlE</button>
         </div>
         <div class="col-md-3 lg-hidden index text-center">
            <button class="useful__btn"><img src="{{ asset('/img/appstore.svg') }}">Загрузить для ApPlE</button>
         </div>
   <div class="categories-info__parallax">
      <div data-relative-input="true" id="scene">
         <div data-depth="0.1" class="categories-info__parallax-el categories-info__parallax-el-1 md-hidden"><img src="{{ asset('/img/paralax1.png') }}"></div>
         <div data-depth="0.1" class="categories-info__parallax-el categories-info__parallax-el-2 md-hidden"><img src="{{ asset('/img/paralax2.png') }}"></div>
         <div data-depth="0.1" class="categories-info__parallax-el categories-info__parallax-el-3"><img src="{{ asset('/img/paralax3.png') }}"></div>
         <div data-depth="0.3" class="categories-info__parallax-el categories-info__parallax-el-4 md-hidden"><img src="{{ asset('/img/paralax4.png') }}"></div>
         <div data-depth="0.3" class="categories-info__parallax-el categories-info__parallax-el-5 md-hidden"><img src="{{ asset('/img/paralax5.png') }}"></div>
         <div data-depth="0.3" class="categories-info__parallax-el categories-info__parallax-el-6"><img src="{{ asset('/img/paralax6.png') }}"></div>
         <div data-depth="0.15" class="categories-info__parallax-el categories-info__parallax-el-7"><img src="{{ asset('/img/paralax7.png') }}"></div>
         <div data-depth="0.2" class="categories-info__parallax-el categories-info__parallax-el-8"><img src="{{ asset('/img/paralax8.png') }}"></div>
      </div>    
   </div>
</div>
</section>