<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <title>Marathon::Back</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    @yield('style')
    <script src="{{ asset('backend/js/require.min.js')}}"></script>
    <script>
      requirejs.config({
          baseUrl: '/'
      });
    </script>
    <link href="{{ asset('backend/css/dashboard.css')}}" rel="stylesheet" />
    <link href="{{ asset('backend/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/js/dashboard.js')}}"></script>
    <link href="{{ asset('backend/plugins/charts-c3/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/charts-c3/plugin.js')}}"></script>
    <link href="{{ asset('backend/plugins/maps-google/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/maps-google/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/input-mask/plugin.js')}}"></script>
  </head>
  <body>
<div class="page">
    <div class="page-main">
    <div class="header py-4">
        <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="/dashboard">
            <img src="{{ asset('img/logo.png') }}"  width="70">
            </a>
            <div class="d-flex order-lg-2 ml-auto">
            <div class="dropdown">
                <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                <span class="avatar" style="    "></span>
                <span class="ml-2 d-none d-lg-block">
                    <span class="text-default">{{ Auth::user()->name }}</span>
                </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                        <button class="dropdown-item" type="submit"><i class="dropdown-icon fe fe-log-out"></i> Sign out</button>
                </form>
                </div>
            </div>
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
            <span class="header-toggler-icon"></span>
            </a>
        </div>
        </div>
    </div>
    <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
        <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
            <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                <li class="nav-item">
                    <a href="{{ action('PanelController@index') }}" class="nav-link {{ request()->is('dashboard*') ? 'active' : '' }}"><i class="fe fe-activity"></i> Статистика</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('PanelController@resources') }}" class="nav-link {{ request()->is('resources*') ? 'active' : '' }}"><i class="fe fe-info"></i> Ресурсы</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('PromoController@index') }}" class="nav-link {{ request()->is('promo*') ? 'active' : '' }}"><i class="fe fe-package"></i> Промокоды</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('DistanceController@index') }}" class="nav-link {{ request()->is('distance*') ? 'active' : '' }}"><i class="fe fe-map-pin"></i> Дистанция</a>
                </li>
                <li class="nav-item">
                    <a href="{{ action('PanelController@pages') }}" class="nav-link {{ request()->is('pages*') ? 'active' : '' }}"><i class="fe fe-airplay"></i> Страницы</a>
                </li>
            </ul>
            </div>
        </div>
        </div>
    </div>
    <div class="container">
        @include('partials.message')
    </div>
    @yield('content')
    </div>
<footer class="footer">
    <div class="container">
    <div class="row align-items-center flex-row-reverse">
        <div class="col-12 mt-3 mt-lg-0 text-center">
        Copyright © 2019. All rights reserved.
        </div>
    </div>
    </div>
    </footer>
</div>
@yield('script')
  </body>
</html>
