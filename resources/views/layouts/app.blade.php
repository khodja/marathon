<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Language" content="{{ app()->getLocale() }}" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <title>Samarkand Half Marathon</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,700,700i,900,900i&display=swap&subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="wrapper">
<nav class="nav">
    <div class="container">
        <div class="col-md-9">
            @if(request()->is('*register*') || request()->is('*about*') || request()->is('*information*') || request()->is('*program*')
            || request()->is('*runners*') || request()->is('*summary*') || request()->is('*auth*') || request()->is('*contacts*')
            )
            <a href="{{action('PageController@index')}}" class="nav__logo">
                Samarkand <br>
                Half Marathon
            </a>
            @endif
            <div class="nav__links">
                <a href="{{ action('PageController@about')  }}">@lang('main.nav.1')</a>
                <a href="{{ action('PageController@information')  }}">@lang('main.nav.2')</a>
{{--                <a href="{{ action('PageController@program')  }}">@lang('main.nav.3')</a>--}}
                <a href="{{ action('PageController@runners')  }}">@lang('main.nav.4')</a>
{{--                <a href="{{ action('PageController@summary')  }}">@lang('main.nav.5')</a>--}}
                <a href="{{ action('PageController@contacts') }}">@lang('main.nav.6')</a>
            </div>
        </div>
        <div class="col-md-3 text-right">
            <div class="nav__profile">
                @if(!Auth::check())
                <button id="profile">
                    <img src="{{ asset('img/profile.svg')  }}" width="40">
                </button>
                @else
                    @if(Auth::user()->is_paid)
                        <a href="{{action('PageController@profile')}}">
                            <img src="{{ asset('img/profile-green.svg')  }}" width="40">
                        </a>
                    @else
                    <a href="{{action('PageController@payment')}}">
                        <img src="{{ asset('img/profile-green.svg')  }}" width="40">
                    </a>
                    @endif
                @endif
        </div>
        <div class="nav__lang">
            <a href="{{ LaravelLocalization::getLocalizedURL('uz') }}">Uz</a>
            <a href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Ru</a>
            <a href="{{ LaravelLocalization::getLocalizedURL('en') }}">En</a>
        </div>
    </div>
    <div class="md-float-right">
        <div class="hamburger toggle">
            <div class="hamburger__container">
              <div class="hamburger__inner"></div>
              <div class="hamburger__hidden"></div>
            </div>
        </div>
    </div>
    </div>
</nav>
<div class="auth-modal">
</div>
                @include('partials.menu')
                @yield('content')
                <div class="site-bottom">
                <div class="container">
                    <div class="col-md-9">
                        <h3 class="site-bottom__title">
                            @lang('main.part')
                        </h3>
                    </div>
                    <div class="col-md-3 text-right">
                        <a href="{{action('PageController@register')}}" class="site-bottom__btn main__register">
                            @lang('main.register')
                        </a>
                    </div>
                </div>
                    @include('partials.footer')
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
            $('#profile').click(function () {
                $('.auth-modal').toggleClass('is-show')
                $('.wrapper').toggleClass('fixed')
                $('.auth').toggleClass('is-show')
            })
            $('.auth-modal').click(function () {
                $('.auth-modal').removeClass('is-show')
                $('.wrapper').removeClass('fixed')
                $('.auth').removeClass('is-show')
            })
            // var offset = $("#profile").offset();
            // var el = $('#profile').width();
            // $(window).on('resize',function () {
            //     $('.auth').css({
            //         'background':'black',
            //         'left': offset.left - el
            //     })
            // })


            $('.hamburger').click(function () {
                $('.toggle').toggleClass('display-none')
                $('.modal').toggleClass('is-show')
                $('.wrapper').toggleClass('fixed')
                $('.menu').toggleClass('is-open')
            })
            $('.modal').click(function () {
                $('.toggle').removeClass('display-none')
                $('.modal').removeClass('is-show')
                $('.wrapper').removeClass('fixed')
                $('.menu').removeClass('is-open')
            })
        </script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript">
          @if(session()->has('success'))
           swal({
              title: "Успешно!",
              text: "{{ session()->get('success') }}",
              icon: "success",
              button: "OK",
          });
          @endif
          @if(session()->has('error'))
           swal({
              title: "Ошибка!",
              text: "{{ session()->get('error') }}",
              icon: "error",
              button: "OK",
          });
          @endif
        @if (session('status'))
           swal({
              title: "Успешно!",
              text: "{{ session('status') }}",
              icon: "info",
              button: "OK",
          });
        @endif
        </script>
    @yield('script')
    </body>
</html>
