<?php

namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExportPaid implements FromView
{
    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $search = $this->data;
        $data = User::where([
            ['is_paid',$search['is_paid']],
            ['distance_id',$search['distance']]])->get();
        return view('exports.users', compact('data'));
    }
}
